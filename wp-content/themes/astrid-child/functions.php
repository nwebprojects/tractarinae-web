<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

//     wp_enqueue_style( '311-style',
//     get_stylesheet_directory_uri() . '/css/311.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     wp_enqueue_style( 'bootstrap-style',
//     get_stylesheet_directory_uri() . '/css/bootstrap.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     wp_enqueue_style( 'desktop-style',
//     get_stylesheet_directory_uri() . '/css/desktop.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     wp_enqueue_style( 'font-awesome-style',
//     get_stylesheet_directory_uri() . '/css/font-awesome.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     wp_enqueue_style( 'FontAwesome-min-style',
//     get_stylesheet_directory_uri() . '/css/FontAwesome.min.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     wp_enqueue_style( 'FontAwesome1-min-style',
//     get_stylesheet_directory_uri() . '/css/FontAwesome1.min.css',
//     array( $parent_style ),
//     wp_get_theme()->get('Version')
//     );

//     // wp_enqueue_style( 'FontAwesome2-min-style',
//     // get_stylesheet_directory_uri() . '/css/FontAwesome2.min.css',
//     // array( $parent_style ),
//     // wp_get_theme()->get('Version')
//     // );

    wp_enqueue_style( 'custom-styling',
    get_stylesheet_directory_uri() . '/css/styles.css',
    array( $parent_style ),
    wp_get_theme()->get('Version')
    );
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

function my_scripts_method() {
//     wp_enqueue_script(
//         'line-1',
//         get_stylesheet_directory_uri() . '/js/Line1.js',
//         array( 'jquery' )
//     );
//     wp_enqueue_script(
//         'line-2',
//         get_stylesheet_directory_uri() . '/js/Line2.js',
//         array( 'jquery' )
//     );
//     wp_enqueue_script(
//         'line-3',
//         get_stylesheet_directory_uri() . '/js/Line3.js',
//         array( 'jquery' )
//     );
//     wp_enqueue_script(
//         'line-4',
//         get_stylesheet_directory_uri() . '/js/Line4.js',
//         array( 'jquery' )
//     );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

function child_remove_parent_function() 
{
    remove_action( 'astrid_footer', 'astrid_footer_credits');
}
add_action( 'wp_loaded', 'child_remove_parent_function' );

function footer_credits() {
	echo '<a href="' . esc_url( __( 'http://tractari-nae.ro/', 'astrid' ) ) . '">';
	 	printf( __( 'Powered by %s', 'astrid' ), 'TractariNae.ro' );
	echo '</a>';
}

add_action( 'astrid_footer', 'footer_credits' );
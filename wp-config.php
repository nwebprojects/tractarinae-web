<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tractarinae_ro');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X7@YL_{7Jk:io?8+]u8My6=?}HJ9&lS*7YmR%1ie$]=G1vnxpsZr;gzOid?Q:anc');
define('SECURE_AUTH_KEY',  ',%v]&r-mvDqkS]I;}aT>|5%P=;{nBir1p(Ja$M`_QF<x?H.gY]Z7a~a4or4w|FO^');
define('LOGGED_IN_KEY',    'z-47r^doYnXpjz7_uSR  7:&+`}Y+w4qc!<BgguTe,.e%kc-lYOO+D+N`L}f~ghW');
define('NONCE_KEY',        '(,VDu~h$3wpV1LiC0nG?x 3n9,E]tgFekLm{t4kU/}~Bf<qzT{wcu{uMaP4HK~Ux');
define('AUTH_SALT',        'i5uJaRfX86{{$_val-[[:lg8lW,OQJ0Jy=M,a;H@%U.c5~;2 Gh,6Shd_#B!y@em');
define('SECURE_AUTH_SALT', ']EI;gz+Yx>nu&?(Mj|7>6]W8:nO8mdW6!|?2uYPX+,ks#EzRD3~rT+xAA.^:: Lj');
define('LOGGED_IN_SALT',   'h!/=I{$]eFS=A:EZdz~[DY4>clQb)G=WF8iBywf>t9nA=M40zyAeG]/+>XpY$Yx.');
define('NONCE_SALT',       '?`vv&_t&R]X-H+;5D[+s#H}b`-wZfuDa* KsRbq##s-M5d[R]7SVF(Hna=;=oxwa');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
